<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Mibosque extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'mibosque';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Pol';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Mi Bosque');
        $this->description = $this->l('We forest ONG');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Module creates custom table-> ps_mibosque: id_customer, date.
     * Main hooks used:
     *      displayCustomerAccount: Add a new link to our module page on my account page
     * 
     *      actionPaymentConfirmation: After a payment has been validated we check if the total
     *      amount of the order is over 50, if it is, we're going to be saving the customer and date.
     */
    public function install()
    {
        Configuration::updateValue('MIBOSQUE_LIVE_MODE', false);
        // SQL for creating module table
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayCustomerAccount') &&
            $this->registerHook('actionPaymentConfirmation');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MIBOSQUE_LIVE_MODE');
        // SQL for eliminating module table
        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMibosqueModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration the module.
     * No configuration needed at the moment.(TODO: check if config would be necessary or remove config option)
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMibosqueModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Form structure.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MIBOSQUE_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MIBOSQUE_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MIBOSQUE_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Input values.
     */
    protected function getConfigFormValues()
    {
        return array(
            'MIBOSQUE_LIVE_MODE' => Configuration::get('MIBOSQUE_LIVE_MODE', true),
            'MIBOSQUE_ACCOUNT_EMAIL' => Configuration::get('MIBOSQUE_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MIBOSQUE_ACCOUNT_PASSWORD' => Configuration::get('MIBOSQUE_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /** 
     * Adds 'Mi bosque' option on user account page, it will redirect the user to the module page.
     */
    public function hookDisplayCustomerAccount()
    {
        $this->context->smarty->assign([
            'module_link' => $this->context->link->getModuleLink('mymodule', 'display')
        ]);

        return $this->display(__FILE__, 'myforest.tpl');
    }

    /** 
     * Triggered when order payment is over 50, if it is we record the customer and the date of the order.
     * Data saved will be used later on modules display page.
     * @param id_order int
     */
    public function hookActionPaymentConfirmation(array $params)
    {
        // Instantiate order so we can access total_paid 
        $order = new Order((int) $params['id_order']);
        
        if ($order->total_paid >= 50) { 
            // If user isn't logged we will use 0 as customer_id, so we know how many guests have contributed.
            $customer_id = empty($this->context->cookie->id_customer) ? 0 : $this->context->cookie->id_customer;
            $date = getdate();
            // TODO: Instead of using Db query class use new class implementing Doctrine (DBAL) to retrieve data as stated on official doc()
            Db::getInstance()->insert('ps_weforest', array(
                'id_customer'      => $customer_id,
                'date' => $date
            ));

        }
    }
}
