<?php
/**
 * Front controller for displaying how much trees the customer and freshly have contributed so far.
 */
class MibosqueDisplayModuleFrontController extends ModuleFrontController
{
    // Guests aren't allowed to enter
    public $auth = true;
    public $guestAllowed = false;

    public function initContent()
    {
        // Here either DBclass or preferably a repository(service) created exlusively for the module to extract customer trees
        // ex: select *, count(*) as total_trees from ps_mibosque where id_customer = id_customer, we have customer id stored on user cookie.
       /* $this->context->smarty->assign(
        array(
            'trees' => $trees, // Retrieved from GET vars
            'total_trees' => $total_trees,
        ));*/

        // Will use the file modules/cheque/views/templates/front/validation.tpl
        $this->setTemplate('module:cheque/views/templates/front/display.tpl');
    }   
}
